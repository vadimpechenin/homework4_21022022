import java.util.*;

public class CountMapIml<T> implements CountMap<T>{
    private ArrayList<T> objectInCountMap = new ArrayList<T>();
    private ArrayList<Integer> countOfObjectsInCountMap = new ArrayList<Integer>();

    @Override
    public void add (T o){
        //Проверка, есть ли такое значение в set
        if (objectInCountMap.contains(o)){
            int index = objectInCountMap.indexOf(o);
            int count = countOfObjectsInCountMap.get(index);
            count++;
            countOfObjectsInCountMap.set(index,count);
        }else{
            //Добавление в arraylistы
            objectInCountMap.add(o);
            countOfObjectsInCountMap.add(1);
        }
    }

    @Override
    public int getCount (T o){
        int count=0;
        if (objectInCountMap.contains(o)){
            int index = objectInCountMap.indexOf(o);
            count = countOfObjectsInCountMap.get(index);
        }
        return count;
    }

    @Override
    public int remove (T o){
        int count=0;
        if (objectInCountMap.contains(o)){
            int index = objectInCountMap.indexOf(o);
            count = countOfObjectsInCountMap.get(index);
            objectInCountMap.remove(index);
            countOfObjectsInCountMap.remove(index);
        }
        return count;
    }

    @Override
    public int size (){
        return objectInCountMap.size();
    }

    @Override
    public void addAll(CountMap source){
        //Пробегаемся по source, ищем совпадения, обновляем веса
        Map sourceMap = source.toMap();
        Map thisMap = this.toMap();
        Set<T> typeOfKeysInSource = sourceMap.keySet();
        for (T elem : typeOfKeysInSource){
            int countSource = source.getCount(elem);
            if (this.objectInCountMap.contains(elem)){
                int countThis = this.getCount(elem);
                int index = this.objectInCountMap.indexOf(elem);
                this.countOfObjectsInCountMap.set(index,countThis+countSource);
            }else{
                this.objectInCountMap.add(elem);
                this.countOfObjectsInCountMap.add(countSource);
            }
        }
    }
    @Override
    public Map toMap(){
        Map<T, Integer> map = new HashMap<>();
        for (int indexOfMap = 0 ; indexOfMap<objectInCountMap.size(); indexOfMap++){
            map.put(objectInCountMap.get(indexOfMap), countOfObjectsInCountMap.get(indexOfMap));
        }
        return map;
    }

    @Override
    public void toMap (Map destination) {
        for (int indexOfMap = 0 ; indexOfMap<objectInCountMap.size(); indexOfMap++){
            destination.put(objectInCountMap.get(indexOfMap), countOfObjectsInCountMap.get(indexOfMap));
        }
    }

    @Override
    public String toString() {
        return "CountMapIml{" +
                "objectInCountMap=" + objectInCountMap +
                ", countOfObjectsInCountMap=" + countOfObjectsInCountMap +
                '}';
    }
}
