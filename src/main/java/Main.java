import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args){

    }

    static int allTaskFunction(){
        System.out.println("Создание countMap, тип Integer");
        CountMap<Integer> map = new CountMapIml<>();
        System.out.println("Добавляем элементы 10 10 5 6 5 10");
        map.add(10);
        map.add(10);
        map.add(5);
        map.add(6);
        map.add(5);
        map.add(10);
        System.out.println("Достаем количество элементов 5  6 10 и 11 (11 отсутствует)");
        System.out.println("Для элемента 5: " + map.getCount(5));
        System.out.println("Для элемента 6: " + map.getCount(6));
        System.out.println("Для элемента 10: " + map.getCount(10));
        System.out.println("Для элемента 11: " + map.getCount(11));
        System.out.println("Удаляем элемент 6");
        map.remove(6);
        System.out.println("Теперь для элемента 6 количество : " + map.getCount(6));
        System.out.println("Создание новую countMap, тип Integer");
        CountMap<Integer> mapTwo = new CountMapIml<>();
        System.out.println("Добавляем элементы 10 5 5 6");
        mapTwo.add(10);
        mapTwo.add(5);
        mapTwo.add(5);
        mapTwo.add(6);
        System.out.println("Объединяем два списка, результат");
        map.addAll(mapTwo);
        System.out.println(map.toString());
        System.out.println("Проверка метода toMap");
        Map<Integer, Integer> destination = new HashMap<>();
        map.toMap(destination);
        System.out.println(destination.toString());
        return map.getCount(10);
    }
}
